"""
    # Script to call rDesktop endlessly via subprocess.call()
    # User, domain and server are to be modified on a per user basis
"""
import subprocess

user = 'a_user'
domain = 'a_domain'
server = 'a_server'

while True:
    subprocess.call(['rdesktop', '-f', '-d', domain, '-u', user, '-r', 'disk:usb0=/media/usb0','-r', 'disk:usb1=/media/usb1', '-r', 'disk:usb2=/media/usb2','-r','disk:usb3=/media/usb3', '-x', '0x8C', server])
